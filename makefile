d := test

start: .docker-start .proxy

stop: .docker-stop
	
.proxy:
	npx localtunnel --port 7777 --subdomain $(d)

.docker-start: .docker-stop
	docker compose up -d

.docker-stop:
	docker compose down

